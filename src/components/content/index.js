import React from 'react'
import ex1 from '../../assets/ex1.jpg'
import ex2 from '../../assets/ex2.jpg'
import ex3 from '../../assets/ex3.jpg'
import ex4 from '../../assets/ex4.jpg'
import ex5 from '../../assets/ex5.jpg'
import Particles from 'react-particles-js';
import { Link } from 'react-router-dom'
import { params } from '../../const'
import './style.css'


const produtos = [
    {
        name: 'Camisa preta',
        src: ex1,
        src2: ex3,
        preço: 45.00,
        id: 1
    },
    {
        name: 'Camisa roxa',
        src: ex2,
        src2: ex5,
        preço: 45.00,
        id: 2
    },
    {
        name: 'Camisa branca',
        src: ex3,
        src2: ex2,
        preço: 45.00,
        id: 3
    },
    {
        name: 'Camisa amarela',
        src: ex4,
        src2: ex1,
        preço: 45.00,
        id: 4
    },
    {
        name: 'Camisa preta',
        src: ex5,
        src2: ex3,
        preço: 45.00,
        id: 1
    },
    {
        name: 'Camisa roxa',
        src: ex1,
        src2: ex2,
        preço: 45.00,
        id: 2
    },
    {
        name: 'Camisa branca',
        src: ex2,
        src2: ex1,
        preço: 45.00,
        id: 3
    },
    {
        name: 'Camisa amarela',
        src: ex3,
        src2: ex5,
        preço: 45.00,
        id: 4
    },
    {
        name: 'camisa preta',
        src: ex4,
        src2: ex5,
        preço: 45.00,
        id: 1
    },
]

function renderCard(i, index) {
    return (
        <Link className='card' style={{ textDecoration: 'none' }} to={`/Product`}>
            <img src={i.src} className='imageTeste' alt='product' />
            <img src={i.src2} className='imageTeste2' alt='product' />
            <div className='infosCard'>
                <b className='name'><i>{i.name}</i></b>
                <b className='price'><i>{`RS ${i.preço},00`}</i></b>
            </div>
        </Link>
    )
}

export default function content() {

    return (
        <div className='content'>
            <Particles
                className='particles'
                params={params}
            />
            {produtos.map((i, index) => renderCard(i, index))}
        </div>
    )
}