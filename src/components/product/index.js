import React from 'react'
import Particles from 'react-particles-js';
import SlipFooter from '../../assets/slipFooter2.svg'
import { params } from '../../const'
import './style.css'

export default function product() {

    return (
        <div className='content'>
            <Particles
                className='particles'
                params={params}
            />
            <div className='footer'>
                <img src={SlipFooter} className='img' alt='footer' />
            </div>
        </div>
    )
}