import React, { useState, useEffect } from 'react'
import SlipLogo from '../../assets/slipLogo.svg'
import ShoppingCart from '../../assets/shoppingCart.svg'
import { Link } from 'react-router-dom'
import './style.css'


export default function Header({ top }) {

    useEffect(() => {
        console.log(top)
    });

    return (
        <div className='header' id='header'>
            <Link to={`/`}>
                <img src={SlipLogo} alt='slip logo' />
            </Link>
            {top ?
                <div className='infoSec'>
                    <h6>MEU PEDIDO</h6>
                    <h6>QUEM SOMOS</h6>
                    <h6>CONTATO</h6>
                    <h6>INSTAGRAM</h6>
                </div>
                :
                <div className='infoSec'>
                    
                </div>
            }
            <div className='rightSide'>
                <b className='loginTXT'><i>ENTRAR </i><i> / </i><i> CADASTRAR</i></b>
                <img src={ShoppingCart} alt='shopping cart' />
            </div>
        </div>
    )
}