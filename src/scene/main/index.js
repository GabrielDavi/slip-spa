import React from 'react'
import Product from '../../components/product'
import Header from '../../components/header'
import Content from '../../components/content'
import SlipFooter from '../../assets/slipFooter2.svg'
import { Route } from 'react-router-dom'
import './style.css'


export default function Main() {
    return (
        <div>
            <Route exact path='/' render={() => (
                <div className='container'>
                    <Header top={true}/>
                    <Content />
                    <div className='footer'>
                        <img src={SlipFooter} className='img' alt='footer' />
                    </div>
                </div>
            )} />
            <Route path='/Product' render={() => (
                <div className='container'>
                    <Header/>
                    <Product />
                    
                </div>
            )} />
        </div>
    )
}